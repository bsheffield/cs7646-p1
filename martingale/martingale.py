""""""
"""Assess a betting strategy.  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
Copyright 2018, Georgia Institute of Technology (Georgia Tech)  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
Atlanta, Georgia 30332  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
All Rights Reserved  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
Template code for CS 4646/7646  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
Georgia Tech asserts copyright ownership of this template and all derivative  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
works, including solutions to the projects assigned in this course. Students  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
and other users of this template code are advised not to share it with others  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
or to make it available on publicly viewable websites including repositories  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
such as github and gitlab.  This copyright statement should not be removed  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
or edited.  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
We do grant permission to share solutions privately with non-students such  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
as potential employers. However, sharing with other current or future  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
students of CS 7646 is prohibited and subject to being investigated as a  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
GT honor code violation.  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
-----do not edit anything above this line---  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
Student Name: Brandon Sheffield
GT User ID: bsheffield7
GT ID: 903312988		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
"""

import numpy as np
import matplotlib.pyplot as plt


def author():
    """  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
    :return: The GT username of the student  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
    :rtype: str  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
    """
    return 'bsheffield7'  # replace tb34 with your Georgia Tech username.


def gtid():
    """  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
    :return: The GT ID of the student  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
    :rtype: int  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
    """
    return 903312988  # replace with your GT ID number


def get_spin_result(win_prob):
    """  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
    Given a win probability between 0 and 1, the function returns whether the probability will result in a win.  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
    :param win_prob: The probability of winning  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
    :type win_prob: float  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
    :return: The result of the spin.  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
    :rtype: bool  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
    """
    result = False
    if np.random.random() <= win_prob:
        result = True
    return result


def simulate(win_prob, is_realistic=False, loops=1000):
    """
    Roulette Spin Pseudo code

    episode_winnings = $0
    while episode_winnings < $80:
        won = False
        bet_amount = $1
        while not won
            wager bet_amount on black
            won = result of roulette wheel spin
            if won == True:
                episode_winnings = episode_winnings + bet_amount
            else:
                episode_winnings = episode_winnings - bet_amount
                bet_amount = bet_amount * 2
    """

    loop_cnt = 0
    episode_winnings = 0
    cash_balance = 256
    winnings = np.zeros(1001)

    while episode_winnings < 80:
        spin_result = False
        bet_amount = 1

        while not spin_result:
            loop_cnt += 1

            # wager bet_amount on black
            spin_result = get_spin_result(win_prob)
            if spin_result:
                episode_winnings = episode_winnings + bet_amount
            else:
                episode_winnings = episode_winnings - bet_amount
                if is_realistic:
                    #corner case to be sure you handle is the situation where the next
                    # bet should be $N, but you only have $M (where M<N).  Bet $M.
                    bet_amount = min(cash_balance + episode_winnings, bet_amount * 2)
                else:
                    bet_amount = bet_amount * 2

            winnings[loop_cnt] = episode_winnings
            if is_realistic and cash_balance + episode_winnings == 0:
                winnings[loop_cnt + 1] = episode_winnings
                return winnings

            if loop_cnt == loops:
                return winnings

    winnings[loop_cnt + 1:] = 80

    return winnings


def run_simulator(win_prob, iterations, is_realistic=False, num_loops=1000):
    """
    Simulate 1000 successive bets on spins of the roulette wheel
    """

    simulations = np.zeros((iterations, num_loops + 1), dtype=np.int)

    for i in range(iterations):
        simulations[i, :] = simulate(win_prob, is_realistic, loops=num_loops)

    return simulations


def figure_one(win_prob):
    """
    Figure 1: Run your simple simulator 10 times and track the winnings, starting from 0 each time.
    Plot all 10 runs on one chart using matplotlib functions. The horizontal (X) axis should range
    from 0 to 300, the vertical (Y) axis should range from -256 to +100. Note that we will not be
    surprised if some of the plot lines are not visible because they exceed the vertical or horizontal scales.

    """

    num_times = 10
    winnings = np.zeros((10, 1000))

    winnings = run_simulator(win_prob, 10)

    plt.figure(1)
    plt.xlim(0, 300)
    plt.ylim(-256, 100)
    plt.xlabel("Iterations")
    plt.ylabel("Winnings")
    plt.legend(loc=4)
    for i in range(num_times):
        plt.plot(winnings[i])
    plt.title("Figure 1")
    plt.savefig("Figure_1.png")


def figure_two(win_prob):
    """
    Figure 2: Run your simple simulator 1000 times. Plot the mean value of winnings for each spin
    using the same axis bounds as Figure 1. Add an additional line above and below the mean at mean
     (+) standard deviation, and mean (-) standard deviation of the winnings at each point.
    """

    winnings = np.zeros((1000, 1000))
    num_times = 1000

    winnings = run_simulator(win_prob, 1000)

    mean_winnings = np.mean(winnings, axis=0)
    std_winnings = np.std(winnings, axis=0)
    pos_std_winnings = mean_winnings + std_winnings
    neg_std_winnings = mean_winnings - std_winnings

    plt.figure(2)
    plt.xlim(0, 300)
    plt.ylim(-256, 100)
    plt.xlabel("Iterations")
    plt.ylabel("Winnings")
    plt.legend(loc=4)
    plt.plot(mean_winnings, label="Mean Winnings")
    plt.plot(pos_std_winnings, label="+ std dev")
    plt.plot(neg_std_winnings, label="- std dev")
    plt.title("Figure 2")
    plt.savefig("Figure_2.png")

    return winnings


def figure_three(winnings):
    """
    Figure 3: Use the same data you used for Figure 2, but plot the median instead of the mean.
    Add an additional line above and below the median at median (+) standard deviation, and
    median (-) standard deviation of the winnings at each point.
    """

    median_winnings = np.median(winnings, axis=0)
    std_winnings = np.std(winnings, axis=0)
    pos_std_winnings = median_winnings + std_winnings
    neg_std_winnings = median_winnings - std_winnings

    plt.figure(3)
    plt.xlim(0, 300)
    plt.ylim(-256, 100)
    plt.xlabel("Iterations")
    plt.ylabel("Winnings")
    plt.legend(loc=4)
    plt.plot(median_winnings, label="Median winnings")
    plt.plot(pos_std_winnings, label="+ std dev")
    plt.plot(neg_std_winnings, label="- std dev")
    plt.title("Figure 3")
    plt.savefig("Figure_3.png")


def experiment_one(win_prob):
    """
    Experiment 1: Explore the strategy and make some charts

    Now we want you to run some experiments to determine how well the betting strategy works. T
    he approach we're going to take is called Monte Carlo simulation where the idea is to
    run a simulator over and over again with randomized inputs and to assess the results in
    aggregate. Skip to the "report" section below to which specific properties of the strategy
    we want you to evaluate.

    For all of the above charts and experiments, if and when the target $80 winnings is reached,
    stop betting and allow the $80 value to persist from spin to spin. """

    figure_one(win_prob)
    winnings = np.zeros((1000, 1000))
    winnings = figure_two(win_prob)
    figure_three(winnings)


def figure_four(win_prob):
    """
    Figure 4: Run your realistic simulator 1000 times. Plot the mean value of winnings for
    each spin using the same axis bounds as Figure 1. Add an additional line above and
    below the mean at mean (+) standard deviation, and mean (-) standard deviation of the winnings at each point.
    """

    winnings = np.zeros((1000, 1000))
    is_realistic = True

    winnings = run_simulator(win_prob, 1000, True)

    mean_winnings = np.mean(winnings, axis=0)
    std_winnings = np.std(winnings, axis=0)
    pos_std_winnings = mean_winnings + std_winnings
    neg_std_winnings = mean_winnings - std_winnings

    plt.figure(4)
    plt.xlim(0, 300)
    plt.ylim(-256, 100)
    plt.xlabel("Iterations")
    plt.ylabel("Winnings")
    plt.legend(loc=4)
    plt.plot(mean_winnings, label="Mean Winnings")
    plt.plot(pos_std_winnings, label="+ std dev")
    plt.plot(neg_std_winnings, label="- std dev")
    plt.title("Figure 4")
    plt.savefig("Figure_4.png")

    return winnings


def figure_five(winnings):
    """
    Figure 5: Use the same data you used for Figure 4, but plot the median instead of
     the mean. Add an additional line above and below the median at median (+) standard deviation,
      and median (-) standard deviation of the winnings at each point.
    """

    median_winnings = np.median(winnings, axis=0)
    std_winnings = np.std(winnings, axis=0)
    pos_std_winnings = median_winnings + std_winnings
    neg_std_winnings = median_winnings - std_winnings

    plt.figure(5)
    plt.xlim(0, 300)
    plt.ylim(-256, 100)
    plt.xlabel("Iterations")
    plt.ylabel("Winnings")
    plt.legend(loc=4)
    plt.plot(median_winnings, label="Median Winnings")
    plt.plot(pos_std_winnings, label="+ std dev")
    plt.plot(neg_std_winnings, label="- std dev")
    plt.title("Figure 5")
    plt.savefig("Figure_5.png")


def experiment_two(win_prob):
    """
    Experiment 2: A more realistic gambling simulator

    You may have noticed that the strategy actually works pretty well, maybe better than
     you expected. One reason for this is that we were allowing the gambler to use an unlimited
      bank roll. In this experiment we're going to make things more realistic by giving the
      gambler a $256 bank roll. If he or she runs out of money, bzzt, that's it. Repeat the
      experiments above with this new condition. Note that once the player has lost all of
      their money (i.e., episode_winnings reaches -256) stop betting and fill that
      number (-256) forward. An important corner case to be sure you handle is the situation
      where the next bet should be $N, but you only have $M (where M<N). Make sure you only
       bet $M. Here are the two charts to create:
    """

    winnings = np.zeros((1000, 1000))
    winnings = figure_four(win_prob)
    figure_five(winnings)


def test_code():
    """  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
    Method to test your code  		  	   		   	 			  		 			     			  	  		 	  	 		 			  		  			
    """

    win_prob = 0.473684  # 1 - (10/9) / (10/9 + 1) American Black bets in Roulette
    np.random.seed(gtid())  # do this only once

    experiment_one(win_prob)
    experiment_two(win_prob)


if __name__ == "__main__":
    test_code()
